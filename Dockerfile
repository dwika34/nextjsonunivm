FROM node:18-alpine 

# Install dependencies only when neede
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install

COPY next.config.js ./next.config.js
COPY jsconfig.json ./jsconfig.json

COPY pages ./pages
COPY public ./public
COPY styles ./styles

CMD ["npm","run","dev"]
